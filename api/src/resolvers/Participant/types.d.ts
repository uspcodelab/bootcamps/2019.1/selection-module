export interface Participant {
  id: string;
  userId: string;
  editionId: string;
  role: string;
}

interface ParticipantInputData {
  userId: string;
  editionId: string;
  role: string;
}

export interface ParticipantInput {
  participant: ParticipantInputData;
}

export interface ParticipantQueryById {
  id: string;
}
