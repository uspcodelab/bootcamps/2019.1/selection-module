
import { Context } from 'koa';

import { Participant, ParticipantQueryById } from 'src/resolvers/Participant/types';

async function getParticipant({ id }: ParticipantQueryById, context: Context): Promise<Participant> {
  const participantString: string = await context.redis.getAsync(id + ':participant');
  const participant: Participant = JSON.parse(participantString);
  console.log(participant);

  return participant;
}

export default getParticipant;
