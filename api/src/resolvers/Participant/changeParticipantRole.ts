import { Context } from 'koa';

import { Participant } from './types.d';

import Event from 'src/resolvers/event';

async function changeParticipantRole(args: any, context: Context): Promise<Participant> {

  const participantString: string = await context.redis.getAsync(`${args.id}:participant`);
  const updatedParticipant: Participant = JSON.parse(participantString);

  updatedParticipant.role = args.role;

  const changedParticipantRole: Event =
    new Event('changedParticipantRole', updatedParticipant);

  context.nats.publish(`participant.${updatedParticipant.id}`, JSON.stringify(changedParticipantRole));

  return updatedParticipant;
}

export default changeParticipantRole;
