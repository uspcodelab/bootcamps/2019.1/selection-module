
import { Context } from 'koa';

import { Participant, ParticipantInput } from 'src/resolvers/Participant/types';

// Queries
import getParticipant from 'src/resolvers/Participant/getParticipant';

// Mutations
import createParticipant from 'src/resolvers/Participant/createParticipant';
import changeParticipantRole from './changeParticipantRole';

export const ParticipantQuery: any = {
  getParticipant(_: any, args: any, context: Context): Promise<Participant> {
    return getParticipant(args, context);
  },
};

export const ParticipantMutation: any = {
  createParticipant(_: any, args: ParticipantInput, context: Context): Promise<Participant> {
    return createParticipant(args, context);
  },
  changeParticipantRole(_: any, args: any, context: Context): Promise<Participant> {
    return changeParticipantRole(args, context);
  },
};
