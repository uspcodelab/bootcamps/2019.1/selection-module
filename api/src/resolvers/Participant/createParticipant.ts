import { Context } from 'koa';

import { Participant, ParticipantInput, ParticipantInputData } from 'src/resolvers/Participant/types';

import Event from 'src/resolvers/event';

async function createParticipant(args: ParticipantInput, context: Context): Promise<Participant> {
  const { userId, editionId, role }: ParticipantInputData = args.participant;

  const participant: Participant = {
    id: userId,
    userId,
    editionId,
    role,
  };

  const createdParticipant: Event =
    new Event('createdParticipant', participant.id);

  const updatedParticipant: Event =
    new Event('updatedParticipant', participant);

  context.nats.publish(`new.participant`, JSON.stringify(createdParticipant));
  context.nats.publish(`participant.${participant.id}`, JSON.stringify(updatedParticipant));

  return participant;
}

export default createParticipant;
