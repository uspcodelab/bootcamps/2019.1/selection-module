import { ParticipantMutation, ParticipantQuery } from 'src/resolvers/Participant';
import { UserMutation, UserQuery } from 'src/resolvers/User';

export default {
  Query: {
    ...UserQuery,
    ...ParticipantQuery,
  },
  Mutation: {
    ...UserMutation,
    ...ParticipantMutation,
  },
};
