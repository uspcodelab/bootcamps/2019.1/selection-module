
import { Context } from 'koa';

import { User, UserQueryById } from 'src/resolvers/User/types';

async function getUser({ id }: UserQueryById, context: Context): Promise<User> {
  const userString: string = await context.redis.getAsync(id + ':user');
  console.log('userString: ', userString);
  const user: User = JSON.parse(userString);
  console.log(user);

  return user;
}

export default getUser;
