import { Context } from 'koa';

import { User, UserInput, UserInputData } from 'src/resolvers/User/types';

import Event from 'src/resolvers/event';

async function createUser(args: UserInput, context: Context): Promise<User> {
  const { name, email }: UserInputData = args.user;

  const user: User = {
    id: '1',
    name,
    email,
  };

  const createdUser: Event =
    new Event('createdUser', user.id);

  // const updatedUserName: Event =
  //   new Event('updatedUserName', user.name);

  // const updatedUserEmail: Event =
  //   new Event('updatedUserEmail', user.email);

  const updatedUser: Event =
    new Event('updatedUser', user);

  context.nats.publish(`new.user`, JSON.stringify(createdUser));
  // context.nats.publish(`user.${user.id}`, JSON.stringify(updatedUserName));
  // context.nats.publish(`user.${user.id}`, JSON.stringify(updatedUserEmail));
  context.nats.publish(`user.${user.id}`, JSON.stringify(updatedUser));

  return user;
}

export default createUser;
