export interface User {
  id: string;
  name: string;
  email: string;
}

interface UserInputData {
  name: string;
  email: string;
}

export interface UserInput {
  user: UserInputData;
}

export interface UserQueryById {
  id: string;
}
