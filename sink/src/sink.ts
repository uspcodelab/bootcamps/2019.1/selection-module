import { Client, Msg } from 'ts-nats';

import natsConnection from 'src/nats';
import redis from 'src/redis';

async function updateUser(id: string, field: string, payload: string): Promise<void> {
  // const userString: string = await redis.getAsync(id);
  // const updatedUser: any = JSON.parse(userString);
  // console.log('userString: ', userString);
  // updatedUser[field] = payload;
  // console.log('SINK.TS/updateUser: ', updatedUser, 'field: ', field, 'payload: ', payload);
  field = field;
  await redis.set(id + ':user', JSON.stringify(payload));
}

async function updateParticipant(id: string, field: string, payload: string): Promise<void> {
  field = field;
  await redis.set(id + ':participant', JSON.stringify(payload));
}

async function setupSink(): Promise<any> {
  console.log('🚀 Sink ready');
  const nats: Client = await natsConnection;

  nats.subscribe('new.user', async (_: any, msg: Msg) => {
    const userCreated: any = JSON.parse(msg.data);
    const id: string = userCreated.payload;
    await redis.set(id + ':user', JSON.stringify({ id }));

    nats.subscribe(`user.${id}`, async (__: any, userMsg: Msg) => {
      const { type, payload }: any = JSON.parse(userMsg.data);
      console.log('type: ', type, ', payload: ', payload);
      const field: string = type
        .replace('updatedUser', '')
        .toLowerCase();
      await updateUser(id, field, payload);
    });
  });
  nats.subscribe('new.participant', async (_: any, msg: Msg) => {
    const participantCreated: any = JSON.parse(msg.data);
    const id: string = participantCreated.payload;
    await redis.set(id + ':participant', JSON.stringify({ id }));

    nats.subscribe(`participant.${id}`, async (__: any, participantMsg: Msg) => {
      const { type, payload }: any = JSON.parse(participantMsg.data);
      console.log('type: ', type, ', payload: ', payload);
      console.log(`Sink, at line 47, logging variable 'participantMsg': ${JSON.stringify(participantMsg)}`);
      const field: string = type
        .replace('updatedParticipant', '')
        .toLowerCase();
      await updateParticipant(id, field, payload);
    });
  });
}

setupSink();
