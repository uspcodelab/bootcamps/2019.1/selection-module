import Redis from 'redis';

import { promisifyAll } from 'bluebird';

console.log('No redis');
const redisClient: any = promisifyAll(Redis.createClient({
  host: process.env.REDIS_HOST,
}));

export default redisClient;
